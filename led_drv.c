/*
 * lcd_drv.c
 *
 *  Created on: 29 mar 2016
 *      Author: pietr343
 */

#include "led_drv.h"

#define R OCR1A
#define G OCR2
#define B OCR1B
#define JUMP 20

void setcolor(int r, int g, int b) {
	///Praca w trybie inverting więc 0 to max
	R = 255 - r;
	G = 255 - g;
	B = 255 - b;
}

void animation() {
	switch (state) {
	case 1:
		R = 0;
		G = 255;
		B = 255;
		i++;
		break;
	case 2:
		R = 255;
		G = 0;
		B = 255;
		i++;
		break;
	case 3:
		R = 255;
		G = 255;
		B = 0;
		i++;
		break;
	case 4:
		R = 255;
		G = 0;
		B = 0;
		i++;
		break;
	case 5:
		R = 0;
		G = 255;
		B = 0;
		i++;
		break;
	case 6:
		R = 0;
		G = 0;
		B = 255;
		i++;
		break;
	case 7:
		R = 0;
		G = 0;
		B = 0;
		i++;
		break;
	case 8:
		R = 255;
		G = 255;
		B = 255;
		i++;
		break;
	}
	_delay_ms(10);
	if (i > 100) {
		i = 0;
		state++;
	}
	if (state > 8)
		state = 1;

}

void animation_fade() {
	switch (state) {
	case 1:
		G = 255;
		B = 255;
		R--;
		i++;
		break;
	case 2:
		G = 255;
		B = 255;
		R++;
		i++;
		break;
	case 3:
		R = 255;
		B = 255;
		G--;
		i++;
		break;
	case 4:
		R = 255;
		B = 255;
		G++;
		i++;
		break;
	case 5:
		R = 255;
		G = 255;
		B--;
		i++;
		break;
	case 6:
		R = 255;
		G = 255;
		B++;
		i++;
		break;
	case 7:
		R = 255;
		G--;
		B--;
		i++;
		break;
	case 8:
		R = 255;
		G++;
		B++;
		i++;
		break;
	case 9:
		R--;
		G = 255;
		B--;
		i++;
		break;
	case 10:
		R++;
		G = 255;
		B++;
		i++;
		break;
	case 11:
		R--;
		G--;
		B = 255;
		i++;
		break;
	case 12:
		R++;
		G++;
		B = 255;
		i++;
		break;
	case 13:
		R--;
		G--;
		B--;
		i++;
		break;
	case 14:
		R++;
		G++;
		B++;
		i++;
		break;
	}
	_delay_ms(4);
	if (i > 254) {
		i = 0;
		state++;
	}
	if (state > 14)
		state = 1;

}

void adc_init() {
	//Uruchomienie ADC, wewnętrzne napiecie odniesienia
	ADCSRA = (1 << ADEN) //ADEN: ADC Enable (uruchomienie przetwornika)
			//Ustawienie preskalera, preskaler= 128
			| (1 << ADFR) | (1 << ADPS0) | (1 << ADPS1) | (1 << ADPS2)
			| (1 << ADSC);
	//ADMUX  =  (1<<REFS1) | (1<<REFS0); //REFS1:0: Reference Selection Bits
	//Internal 2.56V Voltage Reference with external capacitor at AREF pin

}

void timer0_init() {
	TCCR0 = (1 << CS00);  // włącza Timer0
	TIMSK = (1 << TOIE0); // włącza przerwanie "Timer0 Overflow"
}

void timer1_init() {
	TCCR1A |= (1 << COM1A1)	//Zmiana stanu wyjścia OC1A na niski przy porównaniu A
	| (1 << COM1B1)	 //Zmiana stanu wyjścia OC1B na niski przy porównaniu B
			| (1 << COM1A0) | (1 << COM1B0)    //Inverting mode
			| (1 << WGM11);    //Tryb 14 (FAST PWM, TOP=ICR1)

	TCCR1B |= (1 << WGM13) | (1 << WGM12)  //Tryb 14 (FAST PWM, TOP=ICR1)
			| (1 << CS10);               //Brak preskalera

	ICR1 = 255;  //Wartość maksymalna (dla trybu 14)
				 //a więc częstotliwość = CLK/ICR1

}

void timer2_init() {
	TCCR2 |= (1 << WGM20) | (1 << WGM21) | (1 << CS20) | (1 << COM21)
			| (1 << COM20);
	///Fast PWM inverting mode, no prescaler
}

void welcome() {

	R = 255;
	for (i = 255; i > 127; i--) {
		G = i;
		B = i;
		_delay_ms(10);
	}
}

void power_off() {

	while (R < 255 || G < 255 || B < 255) {
		if (R < 255)
			R += 1;
		if (G < 255)
			G += 1;
		if (B < 255)
			B += 1;
		_delay_ms(1);
	}
}

void power_on_mid() {

	while (R != 127 || G != 127 || B != 127) {
		if (R > 127)
			R--;
		else if (R < 127)
			R++;
		if (G > 127)
			G--;
		else if (G < 127)
			G++;
		if (B > 127)
			B--;
		else if (B < 127)
			B++;
		_delay_ms(1);
	}

}

void power_on_max() {

	while (R > 0 || G > 0 || B > 0) {
		if (R > 0)
			R--;
		if (G > 0)
			G--;
		if (B > 0)
			B--;
		_delay_ms(1);
	}

}

void one_color_off(char type) {
	switch (type) {
	case 'r':
		while (R < 255) {
			R += 1;
			_delay_ms(1);
		}
		break;
	case 'g':
		while (G < 255) {
			G += 1;
			_delay_ms(1);
		}
		break;
	case 'b':
		while (B < 255) {
			B += 1;
			_delay_ms(1);
		}
		break;
	}
}

void one_color_on(char type) {
	switch (type) {
	case 'r':
		while (R > 127) {
			R -= 1;
			_delay_ms(1);
		}
		break;
	case 'g':
		while (G > 127) {
			G -= 1;
			_delay_ms(1);
		}
		break;
	case 'b':
		while (B > 127) {
			B -= 1;
			_delay_ms(1);
		}
		break;
	}
}

void show_mode(unsigned char mode) {
	switch (mode) {
	case 0:
		DDRD &= ~(1 << PD6);
		DDRD &= ~(1 << PD7);
		break;

	case 1:
		DDRD |= (1 << PD6);  //zaświca diode
		DDRD &= ~(1 << PD7); //gasi diode
		break;

	case 2:
		DDRD &= ~(1 << PD6);
		DDRD |= (1 << PD7);
		break;

	case 3:
		DDRD |= (1 << PD6);
		DDRD |= (1 << PD7);
		break;
	}
}

void color_up(unsigned char color, unsigned char jump) {
	int i = 0;

	switch (color) {
	case 'r':
		for (i = 0; i < JUMP; i++) {
			R -= 1;
			_delay_ms(5);
		}
		break;
	case 'g':
		for (i = 0; i < JUMP; i++) {
			G -= 1;
			_delay_ms(5);
		}
		break;
	case 'b':
		for (i = 0; i < JUMP; i++) {
			B -= 1;
			_delay_ms(5);
		}
		break;
	}
}

void color_down(unsigned char color, unsigned char jump) {
	int i = 0;

	switch (color) {
	case 'r':
		for (i = 0; i < JUMP; i++) {
			R += 1;
			_delay_ms(5);
		}
		break;
	case 'g':
		for (i = 0; i < JUMP; i++) {
			G += 1;
			_delay_ms(5);
		}
		break;
	case 'b':
		for (i = 0; i < JUMP; i++) {
			B += 1;
			_delay_ms(5);
		}
		break;
	}
}

void save_settings() {
	eeprom_write_byte(&pamiec[0], OCR1A);
	eeprom_write_byte(&pamiec[1], OCR1B);
	eeprom_write_byte(&pamiec[2], OCR2);
}

void read_settings() {
	OCR1A = eeprom_read_byte(&pamiec[0]);
	OCR1B = eeprom_read_byte(&pamiec[1]);
	OCR2 = eeprom_read_byte(&pamiec[2]);
}

void save_mode() {
	eeprom_write_byte(&pamiec[3], mode);
}

void read_mode() {
	mode = eeprom_read_byte(&pamiec[3]);
	show_mode(mode);
}


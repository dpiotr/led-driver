/*
 * lcd_drv.h
 *
 *  Created on: 29 mar 2016
 *      Author: pietr343
 */

#ifndef LED_DRV_H_
#define LED_DRV_H_

#include <avr/delay.h>
#include <avr/eeprom.h>

void setcolor(int,int,int);
void animation();
void animation_fade();
void welcome();
void power_off();
void power_on_mid();
void power_on_max();
void one_color_off(char);
void one_color_on(char);
void show_mode(unsigned char);
void color_up(unsigned char color,unsigned char jump);
void color_down(unsigned char color,unsigned char jump);
void save_settings();
void read_settings();
void save_mode();
void read_mode();

void adc_init();
void timer1_init();
void timer2_init();
void timer0_init();

volatile unsigned char i;	   //zmienna pomocnicza w animacjach
volatile unsigned char state;  //state animacje
volatile unsigned char mode;   //tryb pracy

/* Zmienne globalne z odebranymi danymi */
volatile unsigned char dd_rc5_dane_odebrane;
volatile unsigned char dd_rc5_status;

//Pamięć EEPROM
unsigned char pamiec[4]; ///r,b,g,mode

#endif /* LED_DRV_H_ */

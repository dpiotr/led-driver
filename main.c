/*
 LED Driver
 Dul Piotr 2016
 dulpiotr@gmail.com

 TODO
 Clean code in dd_* files
 */

#include <avr/interrupt.h>
#include "dd_rc5.h"
#include "lcd_drv.h"

#define LED1 (1<<PB1)
#define LED2 (1<<PB2)
#define LED3 (1<<PB3)
#define F_CPU 8000000UL
#define R OCR1A
#define G OCR2
#define B OCR1B
#define JUMP 20							//Skok przy regulacji barw
#define FADE_DURATION 3
#define MODE_COUNT 3
#define KEY (1<<PD5)
#define NIL 0xFF

void seek();
void init();

volatile unsigned char i = 0;
volatile unsigned char state = 1;
volatile unsigned char mode = 0;
volatile unsigned char dd_rc5_dane_odebrane = 0;
volatile unsigned char dd_rc5_status = 0;
volatile unsigned short int r_temp;
volatile unsigned short int g_temp;
volatile unsigned short int b_temp;
volatile unsigned char licznik = 0;
unsigned char pamiec[4]; ///r,b,g,mode

int main(void) {

	init();

	while (1) {

		switch (mode) {

		case 0:
			///Klawisze 1-3
			if (dd_rc5_dane_odebrane == 0x1 || dd_rc5_dane_odebrane == 0x801) {
				if (R == 255) {
					one_color_on('r');
					dd_rc5_dane_odebrane = NIL;
				} else if (R != 255) {
					one_color_off('r');
					dd_rc5_dane_odebrane = NIL;
				}
			} else if (dd_rc5_dane_odebrane == 0x2
					|| dd_rc5_dane_odebrane == 0x802) {
				if (G == 255) {
					one_color_on('g');
					dd_rc5_dane_odebrane = NIL;
				} else if (G != 255) {
					one_color_off('g');
					dd_rc5_dane_odebrane = NIL;
				}
			} else if (dd_rc5_dane_odebrane == 0x3
					|| dd_rc5_dane_odebrane == 0x803) {
				if (B == 255) {
					one_color_on('b');
					dd_rc5_dane_odebrane = NIL;
				} else if (B != 255) {
					one_color_off('b');
					dd_rc5_dane_odebrane = NIL;
				}
			}
			///Klawisze 4-9
			else if ((dd_rc5_dane_odebrane == 0x4
					|| dd_rc5_dane_odebrane == 0x804) && R + JUMP <= 255) {
				color_down('r', JUMP);
				dd_rc5_dane_odebrane = NIL;
			} else if ((dd_rc5_dane_odebrane == 0x4
					|| dd_rc5_dane_odebrane == 0x804) && R + JUMP >= 255) {
				R = 255;
				dd_rc5_dane_odebrane = NIL;
			} else if ((dd_rc5_dane_odebrane == 0x7
					|| dd_rc5_dane_odebrane == 0x807) && 255 - R <= 255 - JUMP) {
				color_up('r', JUMP);
				dd_rc5_dane_odebrane = NIL;
			} else if ((dd_rc5_dane_odebrane == 0x7
					|| dd_rc5_dane_odebrane == 0x807) && 255 - R > 255 - JUMP) {
				R = 0;
				dd_rc5_dane_odebrane = NIL;
			}

			else if ((dd_rc5_dane_odebrane == 0x5
					|| dd_rc5_dane_odebrane == 0x805) && G + JUMP <= 255) {
				color_down('g', JUMP);
				dd_rc5_dane_odebrane = NIL;
			} else if ((dd_rc5_dane_odebrane == 0x5
					|| dd_rc5_dane_odebrane == 0x805) && G + JUMP >= 255) {
				G = 255;
				dd_rc5_dane_odebrane = NIL;
			} else if ((dd_rc5_dane_odebrane == 0x8
					|| dd_rc5_dane_odebrane == 0x808) && 255 - G <= 255 - JUMP) {
				color_up('g', JUMP);
				dd_rc5_dane_odebrane = NIL;
			} else if ((dd_rc5_dane_odebrane == 0x8
					|| dd_rc5_dane_odebrane == 0x808) && 255 - G >= 255 - JUMP) {
				G = 0;
				dd_rc5_dane_odebrane = NIL;
			}

			else if ((dd_rc5_dane_odebrane == 0x6
					|| dd_rc5_dane_odebrane == 0x806) && B + JUMP <= 255) {
				color_down('b', JUMP);
				dd_rc5_dane_odebrane = NIL;
			} else if ((dd_rc5_dane_odebrane == 0x6
					|| dd_rc5_dane_odebrane == 0x806) && B + JUMP >= 255) {
				B = 255;
				dd_rc5_dane_odebrane = NIL;
			} else if ((dd_rc5_dane_odebrane == 0x9
					|| dd_rc5_dane_odebrane == 0x809) && 255 - B <= 255 - JUMP) {
				color_up('b', JUMP);
				dd_rc5_dane_odebrane = NIL;
			} else if ((dd_rc5_dane_odebrane == 0x9
					|| dd_rc5_dane_odebrane == 0x809) && 255 - B >= 255 - JUMP) {
				B = 0;
				dd_rc5_dane_odebrane = NIL;
			}
			///Klawisze specjalne
			else if (dd_rc5_dane_odebrane == 0xC
					|| dd_rc5_dane_odebrane == 0x80C) {
				power_off();
				dd_rc5_dane_odebrane = NIL;
			} else if (dd_rc5_dane_odebrane == 0x20
					|| dd_rc5_dane_odebrane == 0x820) {
				power_on_mid();
				dd_rc5_dane_odebrane = NIL;
			} else if (dd_rc5_dane_odebrane == 0x0
					|| dd_rc5_dane_odebrane == 0x800) {
				power_on_max();
				dd_rc5_dane_odebrane = NIL;
			} else if (dd_rc5_dane_odebrane == 0x10
					|| dd_rc5_dane_odebrane == 0x810) {
				save_settings();
				dd_rc5_dane_odebrane = NIL;
			} else if (dd_rc5_dane_odebrane == 0x11
					|| dd_rc5_dane_odebrane == 0x811) {
				save_mode();
				dd_rc5_dane_odebrane = NIL;
			}
			break;

		case 1:
			ADMUX = 0xC5;	   //PC5
			_delay_ms(10);
			if (ADC / 4 < 250)
				R = ADC / 4;
			else
				R = 255;
			ADMUX = 0xC3;      //PC3
			_delay_ms(10);
			if (ADC / 4 < 250)
				G = ADC / 4;
			else
				G = 255;
			ADMUX = 0xC4;      //PC4
			_delay_ms(10);
			if (ADC / 4 < 250)
				B = ADC / 4;
			else
				B = 255;
			if (dd_rc5_dane_odebrane == 0x10 || dd_rc5_dane_odebrane == 0x810) {
				save_settings();
				dd_rc5_dane_odebrane = NIL;
			} else if (dd_rc5_dane_odebrane == 0x11
					|| dd_rc5_dane_odebrane == 0x811) {
				save_mode();
				dd_rc5_dane_odebrane = NIL;
			}
			break;

		case 2:
			animation_fade();
			if (dd_rc5_dane_odebrane == 0x11 || dd_rc5_dane_odebrane == 0x811) {
				save_mode();
				dd_rc5_dane_odebrane = NIL;
			}
			break;

		case 3:
			animation();
			if (dd_rc5_dane_odebrane == 0x11 || dd_rc5_dane_odebrane == 0x811) {
				save_mode();
				dd_rc5_dane_odebrane = NIL;
			}
			break;
		}

		if (!( PIND & KEY)) {
			// opóźnienie
			_delay_ms(40);
			// ponowne sprawdzenie stanu przycisku - jeśli nadal jest naciśnięty, wykonaj kod
			if (!( PIND & KEY)) {
				seek();
				// oczekiwanie na zwolnienie przycisku
				while (!( PIND & KEY))
					;
			}
		}

		if (dd_rc5_status & DD_RC5_STATUS_DANE_GOTOWE_DO_ODCZYTU) {
			if (dd_rc5_dane_odebrane == 0x21 || dd_rc5_dane_odebrane == 0x821) {
				seek();
				dd_rc5_dane_odebrane = NIL;
			}

		}

		dd_rc5_status &= ~DD_RC5_STATUS_DANE_GOTOWE_DO_ODCZYTU;
		DD_RC5_WLACZ_DEKODOWANIE;
	}

	return 0;
}

void seek() {
	state = 1;  //dla animacji
	i = 0;
	setcolor(0, 0, 0);
	mode++;
	if (mode > MODE_COUNT) {
		read_settings();
		mode = 0;
	}
	show_mode(mode);
}

void init() {

	cli();

	adc_init();
	//timer0_init();
	timer1_init();
	timer2_init();

	DDRC = 0xFF;				//Nieużywane Piny na porcie C jako wyjścia
	DDRC &= ~(1 << PC5);
	DDRC &= ~(1 << PC4);		//Wejścia ADC
	DDRC &= ~(1 << PC3);
	DDRB |= LED1 | LED2 | LED3;   //Wyjścia LED

	//Inicjalizacja rejestrów wartościami
	R = 255;
	G = 255;
	B = 255;

	DDRD &= ~KEY;
	PORTD |= KEY;

	dd_rc5_ini();
	dd_rc5_dane_odebrane = NIL;

	//Globalne uruchomienie przerwań
	sei();

	welcome();

	read_mode();
	if (mode > MODE_COUNT)
		mode = 0;  //domyślny tryb po programowaniu
	state=1;
	i=0;
	read_settings();
}
